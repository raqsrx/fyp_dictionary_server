package lk.apiit.cb006707.fyp_dictionary_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FypDictionaryServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FypDictionaryServerApplication.class, args);
	}

}
