package lk.apiit.cb006707.fyp_dictionary_server.model;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collation = "words")
public class Word {
    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String wordId;
    private int frequency;
    private int length;
    private int numOfVowels;
    private double freqPercentile;
    private double lengthPercentile;
    private double vowelPercentile;
    private double score;

    public String getWordId() {
        return wordId;
    }

    public void setWordId(String wordId) {
        this.wordId = wordId;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getNumOfVowels() {
        return numOfVowels;
    }

    public void setNumOfVowels(int numOfVowels) {
        this.numOfVowels = numOfVowels;
    }

    public double getFreqPercentile() {
        return freqPercentile;
    }

    public void setFreqPercentile(double freqPercentile) {
        this.freqPercentile = freqPercentile;
    }

    public double getLengthPercentile() {
        return lengthPercentile;
    }

    public void setLengthPercentile(double lengthPercentile) {
        this.lengthPercentile = lengthPercentile;
    }

    public double getVowelPercentile() {
        return vowelPercentile;
    }

    public void setVowelPercentile(double vowelPercentile) {
        this.vowelPercentile = vowelPercentile;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
