package lk.apiit.cb006707.fyp_dictionary_server.repository;

import lk.apiit.cb006707.fyp_dictionary_server.model.Word;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WordsRepo extends MongoRepository<Word, String> {
}
