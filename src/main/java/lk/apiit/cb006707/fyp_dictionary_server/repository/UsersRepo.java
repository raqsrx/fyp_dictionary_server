package lk.apiit.cb006707.fyp_dictionary_server.repository;

import lk.apiit.cb006707.fyp_dictionary_server.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UsersRepo extends MongoRepository<User, String> {
}
